<?php

namespace TranslationImportExportBundle\Command;

use TranslationImportExportBundle\Exceptions\DataFilesException;
use TranslationImportExportBundle\Exceptions\ReadFileException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Finder\Finder;

/**
 * Allow to generate a csv table file readable with Excel, with translation keys, french strings and their relative
 * targeted language translation. Translation keys and french strings are from our project. Targeted language
 * translated strings are from a given csv file you need to place in the
 * TranslationImportExportBundle/Resources/CSV. The command will match in our table file correct targeted language
 * strings with our french strings when it's possible. It also exports keys and strings wich are not used in french.
 *
 * Example of an expected given csv table :
 *
 *  *******************************************************
 *  * our french strings  *  our partner's english string *
 *  *******************************************************
 *  *                     *                               *
 *  *                     *                               *
 *  *******************************************************
 *
 * Exported csv table will be :
 *
 *  *********************************************************************************************
 *  * translation keys  *  our french strings  *  our english string  *  english string to come *
 *  *********************************************************************************************
 *  *                   *                      *                      *                         *
 *  *                   *                      *                      *                         *
 *  ******************************************************************************************* *
 *
 * For use, in the command console, type the following command name with the project translation directory name (required
 * argument) and the given csv file name if any (optional).
 * Ex : translation:file:export:data Mo Middle_Office_translation.csv
 * Ex : translation:file:export:data Core
 *
 * Class DataTranslationFileExportCommand
 */
class DataTranslationFileExportCommand extends Command
{
    const CMD_NAME = 'translation:file:export:data';
    const CMD_TRANS_DIR = 'app/Resources/translations/';
    const CMD_EXPORT_DIR = 'src/TranslationImportExportBundle/Export/';
    const CMD_EXPORT_FILE_NAME = 'trad_export_';
    const CMD_CSV_TO_READ_DIR = 'src/TranslationImportExportBundle/Resources/CSV/';
    const CMD_REFERENCE_LANG = 'fr';
    const CMD_TARGETED_LANG = 'en';

    /** @var array french translation keys */
    private $originalFrenchKeys = [];
    /** @var array french translation strings */
    private $originalFrenchStrings = [];
    /** @var array french all translation data */
    private $originalFrenchData = [];
    /** @var array targeted language translation keys */
    private $originalTargetedLangKeys = [];
    /** @var array targeted language translation strings */
    private $originalTargetedLangStrings = [];
    /** @var array targeted language all translation data */
    private $originalTargetedLangData = [];
    /** @var array list of all translation files parsed with the command */
    private $processedFiles = [];
    /** @var bool check if a csv file is give at launch for processing it or not */
    private $isCsvFileGiven = false;

    /** @var array location and name of the given csv */
    private $csvParsingOptions = [
        'finder_in' => self::CMD_CSV_TO_READ_DIR,
        'finder_name' => '',
        'ignoreFirstLine' => true,
    ];

    /** @var array data lines from the given scv */
    private $foundRows = [];
    /** @var array translation table ready for processing our csv file */
    private $csvTransTab = [];

    /**
     * DataTranslationFileExportCommand constructor.
     */
    public function __construct()
    {
        parent::__construct(self::CMD_NAME);
    }

    protected function configure()
    {
        $this->setName(self::CMD_NAME)
            ->addArgument('translation_directory', InputArgument::REQUIRED, 'Which translation directory are you looking for ?')
            ->addArgument('csv_file_to_read', InputArgument::OPTIONAL, 'What is the name of the CSV file to read and compare ?')
            ->setDescription('Export original fr/en translations files from a general translation directory')
            ->setHelp('This command allows you to export original fr/en translations into a csv file')
        ;
    }

    /**
     * @param  InputInterface  $input
     * @param  OutputInterface $output
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dir = $input->getArgument('translation_directory');
        $date = new \DateTime();
        $finalFileName = self::CMD_EXPORT_FILE_NAME.$dir.'_'.self::CMD_TARGETED_LANG.'_'.$date->format('Ymd-His');

        if (!empty($input->getArgument('csv_file_to_read'))) {
            $this->isCsvFileGiven = true;
        }

        if ($this->isCsvFileGiven) {
            $this->parseGivenCsvFile($input);
        }

        if ($this->buildCsvTranslationsTab($dir) && $this->generateCsvFile($finalFileName)) {
            $output->writeln('*************************************');
            $output->writeln('Project translation files parsed : ');
            foreach ($this->processedFiles as $processedFile) {
                $output->writeln($processedFile);
            }
            $output->writeln('**************************************');
            $output->writeln('The file '.$finalFileName.'.csv has been successfully created in '.self::CMD_EXPORT_DIR);
        }

        exit();
    }

    /**
     * Parse the given csv file and set found rows in an array.
     *
     * @param InputInterface $input
     */
    protected function parseGivenCsvFile(InputInterface $input)
    {
        $this->csvParsingOptions['finder_name'] = $input->getArgument('csv_file_to_read');

        $path = $this->csvParsingOptions['finder_in'].$this->csvParsingOptions['finder_name'];

        if (false !== ($handle = fopen($path, 'r'))) {
            while (false !== ($data = fgetcsv($handle, null, ';'))) {
                $this->foundRows[] = $data;
            }
        }
    }

    /**
     *  Builds an array ready for processing a csv file
     *  Ex : array(['translation key 1', 'french string 1', 'english string 1'],
     *             ['translation key 2', 'french string 2', 'english string 2'],
     *             ...]).
     *
     * @param  string $dir the translation project directory
     * @return bool
     */
    protected function buildCsvTranslationsTab(string $dir): bool
    {
        $this->memorizeAllTranslationsData($dir, self::CMD_REFERENCE_LANG);
        $this->memorizeAllTranslationsData($dir, self::CMD_TARGETED_LANG);

        foreach ($this->originalFrenchData as $k => $v) {
            if (array_key_exists($k, $this->originalTargetedLangData)) {
                $this->csvTransTab[] = [
                    'key' => $k,
                    'fr' => $this->originalFrenchData[$k],
                    self::CMD_TARGETED_LANG => $this->originalTargetedLangData[$k],
                    'givenTranslation' => '',
                ];
            } else {
                $this->csvTransTab[] = [
                    'key' => $k,
                    'fr' => $this->originalFrenchData[$k],
                    self::CMD_TARGETED_LANG => '/!\ MISSING KEY - DO NOT TRANSLATE FOR NOW ! /!\\',
                    'givenTranslation' => '/!\ MISSING KEY - DO NOT TRANSLATE FOR NOW ! /!\\',
                ];
            }
        }

        foreach ($this->originalTargetedLangData as $k => $v) {
            if (!array_key_exists($k, $this->originalFrenchData)) {
                $this->csvTransTab[] = [
                    'key' => $k,
                    'fr' => '/!\ KEY IS NOT USED IN FRENCH /!\\',
                    self::CMD_TARGETED_LANG => $this->originalTargetedLangData[$k],
                    'givenTranslation' => '',
                ];
            }
        }

        if ($this->isCsvFileGiven) {
            $this->memorizeGivenTranslatedStrings();
        }

        return true;
    }

    /**
     * Compare french strings from our xlf and given csv to get english translation for our final export.
     */
    protected function memorizeGivenTranslatedStrings()
    {
        foreach ($this->foundRows as $preparedLine) {
            foreach ($this->csvTransTab as $key => $csvRow) {
                $matchingString = in_array($preparedLine[0], $csvRow);
                if ($matchingString && !empty($preparedLine[0])) {
                    $this->csvTransTab[$key]['givenTranslation'] = $preparedLine[1];
                }
            }
        }
    }

    /**
     * set all translation keys and translation strings from original files.
     *
     * @param  string $dir  the targeted translation project directory
     * @param  string $lang the targeted language
     * @return bool
     */
    protected function memorizeAllTranslationsData(string $dir, string $lang): bool
    {
        $languageTranslationsData = $this->getTranslationsDataFromLanguageFiles($dir, $lang);

        if (self::CMD_REFERENCE_LANG === $lang) {
            foreach ($languageTranslationsData['keys'] as $k => $v) {
                $this->originalFrenchData[$v] = $languageTranslationsData['strings'][$k];
            }
            $this->originalFrenchKeys = $languageTranslationsData['keys'];
            $this->originalFrenchStrings = $languageTranslationsData['strings'];

            return true;
        }

        if (self::CMD_TARGETED_LANG === $lang) {
            foreach ($languageTranslationsData['keys'] as $k => $v) {
                $this->originalTargetedLangData[$v] = $languageTranslationsData['strings'][$k];
            }
            $this->originalTargetedLangKeys = $languageTranslationsData['keys'];
            $this->originalTargetedLangStrings = $languageTranslationsData['strings'];

            return true;
        }
    }

    /**
     * Get translation keys and strings from files of a given language & directory.
     *
     * @param  string $dir  the targeted translation project directory
     * @param  string $lang the targeted language
     * @return array
     */
    protected function getTranslationsDataFromLanguageFiles(string $dir, string $lang): array
    {
        try {
            $originalData = $this->getOriginalDataFromFilesInDirectory($dir, $lang);

            $originalLanguageKeys = [];
            $originalLanguageStrings = [];

            foreach ($originalData['keys'] as $file => $keys) {
                foreach ($keys as $val) {
                    $originalLanguageKeys[] = $val;
                }
            }
            foreach ($originalData['strings'] as $file => $strings) {
                foreach ($strings as $val) {
                    $originalLanguageStrings[] = $val;
                }
            }

            $keysIterations = count($originalLanguageKeys);
            $stringsIterations = count($originalLanguageStrings);

            if ($keysIterations == $stringsIterations) {
                return ['keys' => $originalLanguageKeys, 'strings' => $originalLanguageStrings];
            }
            throw new DataFilesException('error detected during the generation of original '.self::CMD_TARGETED_LANG.' keys/strings array : '.$stringsIterations.' string(s) VS '.$keysIterations.' key(s) !');
        } catch (DataFilesException $e) {
            echo $e;
        }
    }

    /**
     * Get translation keys and strings from files for a language in a given directory.
     *
     * @param  string $dir  the targeted translation project directory
     * @param  string $lang the targeted language
     * @return array
     */
    protected function getOriginalDataFromFilesInDirectory(string $dir, string $lang): array
    {
        try {
            $originalFiles = $this->getOriginalFilesPathNamesForLanguage($dir, $lang);

            if (!empty($originalFiles)) {
                $originalKeysPerFile = [];
                $originalStringsPerFile = [];

                foreach ($originalFiles as $file) {
                    $this->processedFiles[] = str_replace(self::CMD_TRANS_DIR, '', $file);
                    $crawler = new Crawler(file_get_contents($file));

                    $originalKeysPerFile[] = $crawler->filter('source')->each(function (Crawler $node) {
                        return $node->text();
                    });

                    $originalStringsPerFile[] = $crawler->filter('target')->each(function (Crawler $node) {
                        return str_replace('  ', '', trim($node->text()));
                    });
                }

                return ['keys' => $originalKeysPerFile, 'strings' => $originalStringsPerFile];
            }
            throw new ReadFileException('Unable to reach translation directory or files translation data.');
        } catch (ReadFileException $e) {
            echo $e;
        }
    }

    /**
     * Retrieve all xlf files names of our project according to a searched language & directory.
     *
     * @param  string $dir  the targeted translation project directory
     * @param  string $lang the targeted locale
     * @return array  $filesPathNames   the xlf files path names
     */
    protected function getOriginalFilesPathNamesForLanguage(string $dir, string $lang): array
    {
        $finder = new Finder();

        $filesPathNames = [];
        $foundOriginalFiles = $finder->files()->in(self::CMD_TRANS_DIR.$dir.'/'.$lang)->name('*.xlf');

        foreach ($foundOriginalFiles as $file) {
            $filesPathNames[] = $file->getPathname();
        }
        sort($filesPathNames);

        return $filesPathNames;
    }

    /**
     * @param  string $finalFileName the name of the processed and exported final file
     * @return bool
     */
    protected function generateCsvFile(string $finalFileName): bool
    {
        $exportFilePath = self::CMD_EXPORT_DIR.$finalFileName.'.csv';
        $csvFile = fopen($exportFilePath, 'w+');
        $headers = ['id_trad',
            'french',
            strtoupper(self::CMD_TARGETED_LANG).' by default',
            'final '.strtoupper(self::CMD_TARGETED_LANG).' translation',
        ];
        // excel encoding for special char :
        fprintf($csvFile, chr(0xEF).chr(0xBB).chr(0xBF));

        fputcsv($csvFile, $headers, ';');

        foreach ($this->csvTransTab as $line) {
            fputcsv($csvFile, $line, ';');
        }

        fclose($csvFile);
        chmod($exportFilePath, 0777);

        return true;
    }
}
