<?php

namespace TranslationImportExportBundle\Command;

use TranslationImportExportBundle\Exceptions\ReadFileException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Finder\Finder;

/**
 * Allow to import translated strings from a given csv table file in our project xlf translation files according to
 * their translation keys. Warning : replace ALL pre existent strings in the xlf files of the targeted language ('en'
 * by default). You need to place the csv file in the TranslationImportExportBundle/Resources/CSV directory.
 *
 * Example of an expected given csv table :
 *
 *  ********************************************************************************************************
 *  * translation keys  *  our french strings  *  our english string  *  english strings from translators  *
 *  ********************************************************************************************************
 *  *                   *                      *                      *                                    *
 *  *                   *                      *                      *                                    *
 *  ********************************************************************************************************
 *
 * For use, in the command console, type the following command name with the project translation directory name
 * and the given csv file name if any (required arguments).
 * Ex : translation:file:import:data Mo Middle_Office_translation.csv
 *
 * Class DataTranslationFileImportCommand.
 */
class DataTranslationFileImportCommand extends Command
{
    const CMD_NAME = 'translation:file:import:data';
    const CMD_CSV_TO_READ_DIR = 'src/TranslationImportExportBundle/Resources/CSV/';
    const CMD_TRANS_DIR = 'app/Resources/translations/';
    const CMD_TARGETED_LANGUAGE = 'en';

    /** @var string special character used to separate columns in the csv file */
    protected $separator = ';';
    /** @var array all translation keys in required found in project files */
    private $originalKeysPerFile = [];

    /** @var array location and name of the given csv */
    private $csvParsingOptions = [
        'finder_in' => self::CMD_CSV_TO_READ_DIR,
        'finder_name' => '',
        'ignoreFirstLine' => true,
    ];

    /** @var array translation keys & english strings from the partner's translators scv */
    private $stringsToImport = [];
    /** @var int total number of strings checked */
    private $iterations = 0;
    /** @var array all keys from the given csv file wich have been processed */
    private $processedKeys = [];

    /**
     * DataTranslationFileImportCommand constructor.
     */
    public function __construct()
    {
        parent::__construct(self::CMD_NAME);
    }

    protected function configure()
    {
        $this->setName(self::CMD_NAME)
            ->addArgument('translation_directory', InputArgument::REQUIRED, 'Which translation directory are you looking for ?')
            ->addArgument('csv_file_to_import', InputArgument::REQUIRED, 'What is the name of the CSV file to import ?')
            ->setDescription('Translation file import')
            ->setHelp('This command allows you to import translations into existing files in a project translation directory')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dir = $input->getArgument('translation_directory');
        $lang = self::CMD_TARGETED_LANGUAGE;

        $this->getStringsInCsvFile($input);
        $this->getOriginalKeysFromFilesInDirectory($dir, $lang);

        if ($this->replaceTargetInOurXlfFiles() && $this->iterations > 0) {
            $output->writeln('Key(s) wich have been processed :');
            foreach ($this->processedKeys as $key) {
                $output->writeln($key);
            }
            $output->writeln('File successfully imported. '.$this->iterations.' key(s) updated.');
        } elseif (0 === $this->iterations) {
            $output->writeln(
                'No string has been imported. Maybe your csv file is empty or doesn\'t match the translation '
                .'directory you choose at launch...');
        }

        exit();
    }

    /**
     * Parse the partener's translators given csv file and set found rows in an array.
     *
     * @param InputInterface $input
     */
    protected function getStringsInCsvFile(InputInterface $input)
    {
        $foundRows = [];
        $this->csvParsingOptions['finder_name'] = $input->getArgument('csv_file_to_import');
        if (preg_match('/\.tsv$/', $this->csvParsingOptions['finder_name'])) {
            $this->separator = "\t";
        }
        $path = $this->csvParsingOptions['finder_in'].$this->csvParsingOptions['finder_name'];

        try {
            if (false !== ($handle = fopen($path, 'r'))) {
                while (false !== ($data = fgetcsv($handle, null, $this->separator))) {
                    $foundRows[] = $data;
                }
                if (empty($foundRows)) {
                    throw new ReadFileException('WARNING : the given csv file is empty ! There is no string to import');
                }

                foreach ($foundRows as $val) {
                    if (!isset($val[3])) {
                        echo "Error reading source column : please check used separator (currently : '{$this->separator}')\n";
                        var_dump($val);
                        echo "\n";
                        exit;
                    }
                    $this->stringsToImport[$val[0]] = \trim($val[3]);
                }
            } else {
                throw new ReadFileException('WARNING : the given csv file can\'t be read. Please, check your file and retry.');
            }
        } catch (ReadFileException $e) {
            echo $e;
        }
    }

    /**
     * Retrieve all xlf files names of our project according to a searched language & directory.
     *
     * @param  string $dir  the targeted translation project directory
     * @param  string $lang the targeted locale
     * @return array  $filesPathNames   the xlf files path names
     */
    protected function getOriginalFilesPathNamesForLanguage(string $dir, string $lang): array
    {
        $finder = new Finder();
        $filesPathNames = [];
        $foundOriginalFiles = $finder->files()->in(self::CMD_TRANS_DIR.$dir.'/'.$lang)->name('*.xlf');

        foreach ($foundOriginalFiles as $file) {
            $filesPathNames[] = $file->getPathname();
        }

        sort($filesPathNames);

        return $filesPathNames;
    }

    /**
     * Retrieve all translation keys in our xlf translation files.
     *
     * @param  string $dir  the targeted translation directory
     * @param  string $lang the targeted language
     * @return bool
     */
    protected function getOriginalKeysFromFilesInDirectory(string $dir, string $lang)
    {
        try {
            $originalFiles = $this->getOriginalFilesPathNamesForLanguage($dir, $lang);

            if (!empty($originalFiles)) {
                foreach ($originalFiles as $file) {
                    $crawler = new Crawler(file_get_contents($file));
                    $this->originalKeysPerFile[$file] = $crawler->filter('source')->each(function (Crawler $node) {
                        return trim($node->text());
                    });
                }

                return true;
            }
            throw new ReadFileException('Unable to reach translation directory or xlf files.');
        } catch (ReadFileException $e) {
            echo $e;
        }
    }

    /**
     * Search target strings in our xlf files and replace them with strings given in the csv and matching with the
     * project translation keys.
     *
     * @return bool
     */
    protected function replaceTargetInOurXlfFiles(): bool
    {
        foreach ($this->originalKeysPerFile as $file => $originalKeys) {
            foreach ($this->stringsToImport as $givenKey => $givenString) {
                if (in_array($givenKey, $originalKeys) && !empty($givenString)) {
                    $fileToModify = new \DOMDocument();
                    $fileToModify->load($file);
                    $fileToModify->encoding = 'utf-8';

                    $targetNode = $this->getTargetNode($fileToModify, $givenKey);
                    $isCdataNode = false;

                    if (!empty($targetNode->nodeValue)) {
                        foreach ($targetNode->childNodes as $targetChild) {
                            if (4 === $targetChild->nodeType) {
                                $isCdataNode = true;
                            }
                        }

                        if ($isCdataNode) {
                            $cdataString = $fileToModify->createCDATASection($givenString);
                            $targetNode->nodeValue = '';
                            $targetNode->appendChild($cdataString);
                        } else {
                            $targetChild = $this->cleanNodeValue($targetChild);

                            if (empty($targetChild->nodeValue)) {
                                $targetChild->nodeValue = '';
                            } else {
                                $targetChild->nodeValue = $givenString;
                            }
                        }

                        ++$this->iterations;
                        $fileToModify->save($file);
                    } else {
                        $targetNode->nodeValue = $givenString;
                        $fileToModify->save($file);
                        ++$this->iterations;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Return a target node with translated string in value from the DOMDocument of our project translation file.
     *
     * @param  \DOMDocument $file     the translation filepath of our project
     * @param  string       $givenKey the translation key of the given csv file to import
     * @return \DOMNode     the target node following the source node with translation key in value
     */
    private function getTargetNode(\DOMDocument $file, string $givenKey): \DOMNode
    {
        $sourcesList = $file->getElementsByTagName('source');
        foreach ($sourcesList as $source) {
            if ($source->nodeValue === $givenKey) {
                $this->processedKeys[] = $givenKey;

                return $source->nextSibling->nextSibling;
            }
        }
    }

    /**
     * Delete invisible special characters of a node.
     *
     * @param  \DOMNode $node
     * @return \DOMNode
     */
    private function cleanNodeValue(\DOMNode $node): \DOMNode
    {
        $node->nodeValue = str_replace("\n", '', $node->nodeValue);
        $node->nodeValue = str_replace("\t", '', $node->nodeValue);
        $node->nodeValue = str_replace('  ', '', $node->nodeValue);

        return $node;
    }
}
