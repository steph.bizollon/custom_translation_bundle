<?php
/**
 * Created by PhpStorm.
 * User: Steph
 * Date: 16/02/2019
 * Time: 18:21.
 */

namespace TranslationImportExportBundle\Exceptions;

class DataFilesException extends \Exception
{
}
