<?php
/**
 * Created by PhpStorm.
 * User: Steph
 * Date: 16/02/2019
 * Time: 18:22.
 */

namespace TranslationImportExportBundle\Exceptions;

class ReadFileException extends \Exception
{
}
